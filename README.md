# Markov music

Generates probabilistic Markov-chain music, using Python 3 and [Nyquist](https://www.cs.cmu.edu/~music/nyquist/)

## how to use

1. [install Nyquist](https://sourceforge.net/projects/nyquist/)
2. install python 3
3. Create a adjacency-matrix representation (with 2D tuple / list) of the Markov chain, where the states are the pitches / frequencies (see chain.py for example)
4. Create a map from indices to pitches / frequencies (1D tuple / list) (see chain.py for example). A special state with the name `'0'` represents skipping a beat.
5. Run `gen_Markov(chain, keys, start_state_index, n_result_notes, note_dur, instrument)` to get a string. Instruments currently supported are `'osc'`, `'pluck'`, `'saw'`, `'square'`
6. In Nyquist, load mm.sal, and run the string

## development

- Currently, the generated music uses `osc` sine wave oscillators, and all notes have the same duration.
- gen_sal.py is in fact more general, and allows 5 degrees of freedom for each note: pitch/frequency, note duration, oscillator, volume, and start timing of next note
- mm.sal currently supports `my-osc` and `my-hzosc`
- Currently, the Markov chain only supports single-note pitch-to-pitch probabilities. This can be generalized.
