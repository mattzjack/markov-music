# input: a list of tuples
# [(pitch=c4, dur=1, instrument=osc, vol=1, next_start=dur)]
def gen(l):
  result = ['play seq(']
  for x in l:
    pitch, dur, instrument, vol, next_start = 'c4', 1, 'osc', 1, 1
    if len(x) >= 1:
      pitch = x[0]
    if len(x) >= 2:
      dur = x[1]
      next_start = dur
    if len(x) >= 3:
      instrument = x[2]
    if len(x) >= 4:
      vol = x[3]
    if len(x) >= 5:
      next_start = x[4]
    if pitch == '0':
      vol = 0
    result.append('my-{}({}, {}, {}, {})'.format(instrument, pitch, dur, vol, next_start))
    result.append(', ')
  result.pop()
  result.append(')')
  return ''.join(result)
