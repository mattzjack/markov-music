from gen_sal import *
from random import random

# chain: n*n matrix
# entry at row i, col j: Pr{transition from state i to state j}
chain = ((0.2, 0.2, 0.5, 0.1),
         (0.1, 0.6, 0.2, 0.1),
         (0.5, 0.2, 0.2, 0.1),
         (0.3, 0.3, 0.3, 0.1))

# len n
# entry[i]: pitch / frequency for state i
keys = ['c4', 'd4', 'e4', '0'] # 0 means silent

def gen_Markov(chain, keys, start_state, n=72, dur=0.2, instrument='pluck'):
  result = []
  x = start_state
  for _ in range(n):
    result.append((keys[x], dur, instrument))
    probs = chain[x]
    jump = random()
    for i in range(len(probs)):
      if jump < probs[i]:
        x = i
        break
      jump -= probs[i]
  return gen(result)

print(gen_Markov(chain, keys, 0, instrument='saw'))
